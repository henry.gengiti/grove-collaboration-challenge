/*global describe, it, before, beforeEach, after, afterEach */

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const should = chai.should();

const schedule = require('../api/schedule/schedule');

chai.use(chaiHttp);

describe('schedule', () => {

    describe('/GET schedule', () => {
        it('it should GET all the schedules', (done) => {
            chai.request(server.app)
                .get('/api/v1/schedule')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        });
    });
});
/*jslint es6 */
/*jslint node: true */
const express = require('express');
const app = express();
const http = require("http");
const server = http.createServer(app);
const config = require('./config/config');

app.use(express.static(__dirname + "/../../../grove-client/dist/"));

// config app routes
require('./routes/app.routes')(app, express);

// start server on a port
server.listen(config.PORT, "0.0.0.0", function () {
    "use strict";
    console.log("listening on *:" + config.PORT);
});

module.exports.app = app;
module.exports.http = http;
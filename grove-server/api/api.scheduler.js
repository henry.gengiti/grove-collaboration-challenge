const schedule = require('./schedule/schedule');

module.exports = function (express) {
    "use strict";
    const router = new express.Router();

    router.route('/schedule').get(schedule.GET);

    return router;
}
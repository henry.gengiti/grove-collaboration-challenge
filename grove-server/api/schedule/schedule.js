/*jslint es6 */
const https = require('https');
const config = require('../../config/config');
const parser = require('cron-parser');
const _ = require('lodash');
const moment = require('moment');

module.exports = {
    GET: function (req, res, next) {
        "use strict";

        https.get(config.SCHEDULER_HOST + '/schedule', (groveRes) => {
            if (groveRes.statusCode != 200) {
                return res.status(groveRes.statusCode).send({ error: groveRes.statusCode + " error occurred" });
            }

            groveRes.on('data', (d) => {
                const jsonData = JSON.parse(d).data;

                let allTasks = {};
                allTasks.cronList = getAllTasks(jsonData);
                allTasks.futureTaskList = getAllFutureTasks(allTasks.cronList);
                allTasks.pastTaskList = getAllPastTasks(allTasks.cronList);

                res.json(allTasks);
            });

        }).on('error', (e) => {
            console.error(e);
        });
    }
}

function getAllTasks(jsonData) {
    "use strict";

    jsonData.forEach(x => {
        var interval = parser.parseExpression(x.attributes.cron);
        x['past'] = interval.prev();
        x['pastDisplay'] = interval.prev().toString();
        x['future'] = interval.next();
        x['futureDisplay'] = interval.next().toString();
        x['current'] = moment(new Date());
    });

    return jsonData;
}

function getAllPastTasks(scheduleList) {
    "use strict";

    // setting past task list
    let pastTaskList = scheduleList.filter(x => {
        const pastMoment = moment(x.pastDisplay);
        const past3HoursMoment = moment(new Date()).subtract(3,'hours');
        console.log('compare past task pastMoment', pastMoment, ' current ', x.current);
        return (pastMoment.valueOf() < x.current.valueOf() 
                && pastMoment.valueOf() > past3HoursMoment);
    });

    return pastTaskList.sort(function (a, b) {
        return new Date(b.past) - new Date(a.past);
    });
}

function getAllFutureTasks(scheduleList) {
    "use strict";

    // setting past task list
    let futureTaskList = scheduleList.filter(x => {
        const futureMoment = moment(x.futureDisplay);
        const future24Moment = moment(new Date()).add(1,'days');
        console.log('compare future task pastMoment', futureMoment, ' current ', x.current);
        return (futureMoment.valueOf() >= x.current.valueOf() 
                && futureMoment.valueOf() < future24Moment
                );
    });

    return futureTaskList.sort(function (a, b) {
        return new Date(b.future) - new Date(a.future);
    });
}
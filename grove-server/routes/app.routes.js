module.exports = function (app, express) {
    "use strict";

    //expose public path
    app.use(express.static(__dirname + "/../../grove-client/dist/grove-client/"));

    //scheduler API
    app.use('/api/v1', require('../api/api.scheduler')(express));

    // serve ui
    app.use('/', function (req, res) {
        res.sendFile(__dirname + "/../../grove-client/dist/grove-client/index.html");
    });
}
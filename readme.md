# Grove Collaborative - Coding challenge: #

## Instructions to run: ##

* Install nodejs - 8.11.3
* cd into grove-client
    * run the command "npm install"
    * run the commnd "npm run build"
    * *Note* - if you running into issues , run  "npm install -g angular-cli@6.0.8"
* cd into grove-server
    * run the command "npm install"
    * run the command "npm run start"
* open chrome browser and enter --> http://127.0.0.1:9000    

## Instructions to Test:
* **Server Side Tests** - 
    * cd into grove-server and run "npm run test"
* **Client Side Tests (INCOMPLETED)** -
    * cd into grove-client and run "npm run test"

## Assumptions:

* Show in a grid format, all the schedules
* Show in a grid format, tasks scheduled for the next 24 hours
* Show in a grid format, tasks completed in the last 3 hours
* Integrated web notification API to trigger browser notification.
    * Function fires every 5 seconds after the application loaded. 
    * Additional logic to check with the corresponding task to be notified through web notification api is not completed within the given amount of time.
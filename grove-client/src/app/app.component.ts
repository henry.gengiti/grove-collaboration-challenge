import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { PushNotificationsService } from './service/notification-service.service';
import { timeout } from 'q';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [PushNotificationsService]
})
export class AppComponent {
  scheduleList: any = [];
  futureTaskList: any = [];
  pastTaskList: any = [];
  currentDateTime = moment(new Date());

  constructor(private http: HttpClient,
    private _notificationService: PushNotificationsService) {
    this.http.get('/api/v1/schedule').subscribe((data) => {
      const resData = Object.values(data);
      this.scheduleList = resData[0];
      this.futureTaskList = resData[1];
      this.pastTaskList = resData[2];
    }, (err) => {
      console.error(err);
      alert('error occurred');
    });

    this._notificationService.requestPermission();

    // check if a task is ready
    setInterval(() => {
      // TODO: more logic is required to validate if the task is ready.
      // showing that browser notification is alerted.
      this.notify('This is a notification');
    }, 5000);
  }

  notify(msg) {
    const data: Array<any> = [];
    data.push({
      'title': 'Task Notification',
      'alertContent': msg
    });
    this._notificationService.generateNotification(data);
  }
}
